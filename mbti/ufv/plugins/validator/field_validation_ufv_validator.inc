<?php

/**
 * @file
 * Field validation php validator.
 */
$plugin = array(
  'label'       => t('User field required'),
  'description' => t("User field required"),
  'handler'     => array(
    'class' => 'field_validation_ufv_validator',
  ),
);

/**
 * Validator class as plugin of field_validation.
 */
class field_validation_ufv_validator extends field_validation_validator {

  /**
   * Validate field.
   */
  public function validate() {

    global $user;

    $is_for_check = FALSE;

    foreach ($user->roles as $user_role_id => $user_role) {
      if (in_array($user_role_id, $this->rule->settings['user_roles'])) {
        $is_for_check = TRUE;
      }
    }

    if (trim($this->value) == '' && $is_for_check && empty($_GET['pass-reset-token'])) {
      $this->set_error();
    }

    return TRUE;
  }

  /**
   * Provide settings option.
   */
  private function settings_form(&$form, &$form_state) {
    $default_settings = $this->get_default_settings($form, $form_state);

    $roles = user_roles();

    $form['settings']['user_roles'] = array(
      '#title'         => t('Roles'),
      '#type'          => 'select',
      '#multiple'      => TRUE,
      '#options'       => $roles,
      '#default_value' => isset($default_settings['user_roles']) ? $default_settings['user_roles']
      : NULL,
    );
    parent::settings_form($form, $form_state);
  }

}
