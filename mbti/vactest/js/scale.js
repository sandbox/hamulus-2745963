/**
 * @file
 * JS for the module.
 */

(function ($, Drupal) {
  // Use strict mode to reduce development errors.
  // @link http://www.nczonline.net/blog/2012/03/13/its-time-to-start-using-javascript-strict-mode/
  'use strict';
  function is_numeric(mixed_var) {
    return (mixed_var === '') ? false : !isNaN(mixed_var);
  }

  Drupal.behaviors.vactest = {
    attach: function (context, settings) {

      $('.axis-help-title').each(function () {
        $(this).once(function () {

          $(this).click(function () {

            $(this).parent().find('.axis-help-contents').slideToggle();

          });
        });

      });

      $('.jquery-ui-scale-wrapper').each(function () {

        var min = $(this).data('min');
        var max = $(this).data('max');
        var defaultValMin = $(this).data('default-min');
        var defaultValMax = $(this).data('default-max');
        var ranged = false;
        var values = [defaultValMin];

        var thisMin = $(this).find('input.input-min');

        if (is_numeric(defaultValMax + 300)) {
          ranged = true;
          values.push(defaultValMax);
          var thisMax = $(this).find('input.input-max');
        }

        $(this).find('.jquery-ui-scale').slider({
          orientation: 'horisontal',
          range: ranged,
          min: min,
          max: max,
          values: values,
          slide: function (event, ui) {
            thisMin.val(ui.values[0]);
            if (typeof (thisMax) != 'undefined') {
              thisMax.val(ui.values[1]);
            }
          }
        });

      });

      $('.hb').each(function () {

        var minput = $(this).closest('.jquery-ui-scale-wrapper').find('.input-min');
        var maxput = $(this).closest('.jquery-ui-scale-wrapper').find('.input-max');

        var min = $(this).closest('.jquery-ui-scale-wrapper').data('min');
        var max = $(this).closest('.jquery-ui-scale-wrapper').data('max');

        var defaultValMin = $(this).closest('.jquery-ui-scale-wrapper').data('default-min');
        var defaultValMax = $(this).closest('.jquery-ui-scale-wrapper').data('default-max');

        $(this).once(function () {

          if ($(this).hasClass('slider-legend-left') && defaultValMax === 0) {
            $(this).find('span').addClass('btn-success');
          }

          if ($(this).hasClass('slider-legend-center') && max === defaultValMax && min === defaultValMin) {
            $(this).find('span').addClass('btn-success');
          }

          if ($(this).hasClass('slider-legend-right') && defaultValMin === 0) {
            $(this).find('span').addClass('btn-success');
          }

          $(this).click(function () {

            $(this).closest('.jquery-ui-scale-wrapper').find('.btn').removeClass('btn-success');
            $(this).find('.btn').addClass('btn-success');

            if ($(this).hasClass('slider-legend-left')) {
              maxput.val(0);
              minput.val(min);
            }

            if ($(this).hasClass('slider-legend-center')) {
              maxput.val(max);
              minput.val(min);
            }

            if ($(this).hasClass('slider-legend-right')) {
              maxput.val(max);
              minput.val(0);
            }

          });

        });

      });

    }
  };

})(jQuery, Drupal);
