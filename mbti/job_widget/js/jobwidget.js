/**
 * @file
 * Job widget JS functionality.
 */

(function ($, Drupal) {
  // Use strict mode to reduce development errors.
  // @link http://www.nczonline.net/blog/2012/03/13/its-time-to-start-using-javascript-strict-mode/
  'use strict';

  $(document).ready(function () {

    function applyButtons() {

      var index = 0;
      $('.job-buttons').html('');
      $('.field-name-field-user-job-history .field-multiple-table tbody tr').each(function () {

        var title = Drupal.checkPlain($(this).find('.field-name-field-job-title input').val());

        if (!title) {
          title = Drupal.t('New job');
        }

        $('.job-buttons').append('<a data-index="' + index + '" href="javascript:" class="jh-button col-lg-5 btn btn-primary">' + title + '</a>');

        index++;

      });

      $('.field-name-field-user-job-history .field-multiple-table tbody tr').addClass('hidden');
      $('.field-name-field-user-job-history .field-multiple-table tbody tr:last-child').removeClass('hidden');

      $('.field-name-field-user-job-history .field-multiple-table tbody tr:last-child .jh-button:last-child').addClass('active');

      if (typeof ($('.job-addmore [name="field_user_job_history_add_more"]').html()) == 'undefined') {

        var addmoreButton = $('[name="field_user_job_history_add_more"]').detach();
        $('.job-addmore').html('');

        $('.job-addmore').append(addmoreButton);

      }

      $('.jh-button').each(function () {

        $(this).once(function () {
          $(this).click(function () {

            var thisindex = $(this).data('index');

            $('.field-name-field-user-job-history .field-multiple-table tbody tr').addClass('hidden');

            $('[name="field_user_job_history[und][' + thisindex + '][field_job_title][und][0][value]"]').closest('tr').removeClass('hidden');

            $('.jh-button').removeClass('active');
            $(this).addClass('active');

          });
        });

      });

      $('.field-name-field-job-title input').each(function () {
        var thisindex = $(this).attr('name').split('field_user_job_history[und][')[1].split('][field_job_title][und][0][value]')[0];

        $(this).keyup(function () {

          var thisval = Drupal.checkPlain($(this).val());
          if (!thisval) {
            thisval = Drupal.t('New job');
          }

          $('.jh-button[data-index="' + thisindex + '"').html(thisval);
        });
      });

    }

    applyButtons();

    $(document).ajaxStop(function () {
      applyButtons();

    });

  });
})(jQuery, Drupal);
