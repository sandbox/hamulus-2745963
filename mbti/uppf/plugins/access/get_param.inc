<?php

/**
 * @file
 * Plugin to provide access control based upon node type.
 */

$plugin = array(
  'title'                  => t("GET parameter"),
  'description'            => t('Control access by get parameter.'),
  'callback'               => 'ctools_get_param_ctools_access_check',
  'default'                => array('type' => array()),
  'settings form'          => 'ctools_get_param_ctools_access_settings',
  'settings form submit'   => 'ctools_get_param_ctools_access_settings_submit',
  'settings form validate' => 'ctools_get_param_ctools_access_settings_validate',
  'summary'                => 'ctools_get_param_ctools_access_summary',
);

/**
 * Settings form for the 'by get_param' access plugin.
 */
function ctools_get_param_ctools_access_settings($form, &$form_state, $conf) {

  $form['settings']['param'] = array(
    '#title'         => t('GET parameter'),
    '#type'          => 'textfield',
    '#default_value' => $conf['param'] ? $conf['param'] : '',
    '#size'          => 10,
  );

  $form['settings']['operator'] = array(
    '#title'         => t('Operator'),
    '#type'          => 'select',
    '#default_value' => $conf['operator'] ? $conf['operator'] : '',
    '#options'       => array(
      'equal'    => t('Equal'),
      'notequal' => t('Not equal'),
      'empty'    => t('Empty'),
      'notempty' => t('Not empty'),
    ),
  );

  $form['settings']['value'] = array(
    '#title'         => t('Value'),
    '#type'          => 'textfield',
    '#default_value' => $conf['value'] ? $conf['value'] : '',
    '#size'          => 10,
  );
  return $form;
}

/**
 * Clears and saves NID.
 */
function ctools_get_param_ctools_access_settings_submit($form, &$form_state) {

}

/**
 * Validates NID to be sure that it is an integer.
 */
function ctools_get_param_ctools_access_settings_validate($form, &$form_state) {
  if (intval($form_state['values']['settings']['nid']) <= 0) {

  }
}

/**
 * Check for access.
 */
function ctools_get_param_ctools_access_check($conf, $context) {
  // As far as I know there should always be a context at this point, but this
  // is safe.
  $param = $conf['param'];

  $value = $conf['value'];

  if ($conf['operator'] == 'empty' && !isset($_GET[$param])) {
    return TRUE;
  }

  if ($conf['operator'] == 'notempty' && isset($_GET[$param])) {
    return TRUE;
  }

  if ($conf['operator'] == 'equal' && isset($_GET[$param]) && $_GET[$param] == $value) {
    return TRUE;
  }

  if ($conf['operator'] == 'notequal' && isset($_GET[$param]) && $_GET[$param] !=
      $value) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Provide a summary description based upon the get_param.
 */
function ctools_get_param_ctools_access_summary($conf, $context) {
  return t('Returns true if GET parameter %param is %operator to %value',
      array(
        '%param' => $conf['param'],
        '%operator' => $conf['operator'],
        '%value' => $conf['value'],
      ));
}
