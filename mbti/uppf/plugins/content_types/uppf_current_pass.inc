<?php

/**
 * @file
 * Content type plugin to show current pass field of user edit form.
 */

$plugin = array(
  'title'            => t('User current_pass edit field'),
  'description'      => t('User current_pass edit field'),
  'single'           => TRUE,
  'content_types'    => array('uppf_current_pass'),
  'render callback'  => 'uppf_current_pass_content_type_render',
  'category'         => array(t('Form'), -9),
  'edit form'        => 'uppf_current_pass_content_type_edit_form',
  'required context' => new ctools_context_required(t('User'), 'user'),
);

/**
 * Settings form.
 */
function uppf_current_pass_content_type_edit_form($form, &$form_state) {

  return $form;
}

/**
 * Settings form submit.
 */
function uppf_current_pass_content_type_edit_form_submit($form, &$form_state) {

}

/**
 * Settings form validate.
 */
function uppf_current_pass_content_type_edit_form_validate($form, &$form_state) {

}

/**
 * Render callback function.
 */
function uppf_current_pass_content_type_render($subtype, $conf, $args, $context) {

  $block = new stdClass();
  if (isset($context->form) && isset($context->form['account']['current_pass'])) {

    $block->content = array();

    $block->content['account']['current_pass'] = $context->form['account']['current_pass'];
    $block->content['account']['current_pass']['#attributes']['placeholder'] = $block->content['account']['current_pass']['#title'];
    unset($block->content['account']['current_pass']['#title']);
    unset($context->form['account']['current_pass']);
  }

  return $block;
}
