<?php

/**
 * @file
 * Content type plugin to show notify settings field of user edit form.
 */

$plugin = array(
  'title'            => t('User notify edit field'),
  'description'      => t('User notify edit field'),
  'single'           => TRUE,
  'content_types'    => array('uppf_notify'),
  'render callback'  => 'uppf_notify_content_type_render',
  'category'         => array(t('Form'), -9),
  'edit form'        => 'uppf_notify_content_type_edit_form',
  'required context' => new ctools_context_required(t('User'), 'user'),
);

/**
 * Settings form.
 */
function uppf_notify_content_type_edit_form($form, &$form_state) {

  return $form;
}

/**
 * Settings form submit.
 */
function uppf_notify_content_type_edit_form_submit($form, &$form_state) {

}

/**
 * Settings form validate.
 */
function uppf_notify_content_type_edit_form_validate($form, &$form_state) {

}

/**
 * Render callback function.
 */
function uppf_notify_content_type_render($subtype, $conf, $args, $context) {

  $block = new stdClass();
  if (isset($context->form)) {

    $block->content = array();

    $block->content['account']['notify'] = $context->form['account']['notify'];
    unset($context->form['account']['notify']);
  }

  return $block;
}
