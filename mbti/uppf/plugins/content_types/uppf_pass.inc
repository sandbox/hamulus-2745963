<?php

/**
 * @file
 * Content type plugin to show pass field of user edit form.
 */

$plugin = array(
  'title'            => t('User pass edit field'),
  'description'      => t('User pass edit field'),
  'single'           => TRUE,
  'content_types'    => array('uppf_pass'),
  'render callback'  => 'uppf_pass_content_type_render',
  'category'         => array(t('Form'), -9),
  'edit form'        => 'uppf_pass_content_type_edit_form',
  'required context' => new ctools_context_required(t('User'), 'user'),
);

/**
 * Settings form.
 */
function uppf_pass_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['placeholder'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use placeholder instead of label'),
    '#default_value' => !empty($conf['placeholder']) ? $conf['placeholder'] : 0,
  );
  return $form;
}

/**
 * Settings form submit.
 */
function uppf_pass_content_type_edit_form_submit($form, &$form_state) {
  foreach (element_children($form) as $key) {
    if (!empty($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Settings form validate.
 */
function uppf_pass_content_type_edit_form_validate($form, &$form_state) {

}

/**
 * Render callback function.
 */
function uppf_pass_content_type_render($subtype, $conf, $args, $context) {

  $block = new stdClass();
  if (isset($context->form)) {

    $block->content = array();

    $block->content['account']['pass'] = $context->form['account']['pass'];

    if ($conf['placeholder']) {

      $block->content['account']['pass']['pass1']['#attributes']['placeholder'] = $block->content['account']['pass']['pass1']['#title'];
      unset($block->content['account']['pass']['pass1']['#title']);

      $block->content['account']['pass']['pass2']['#attributes']['placeholder'] = $block->content['account']['pass']['pass2']['#title'];
      unset($block->content['account']['pass']['pass2']['#title']);
    }

    unset($context->form['account']['pass']);
  }

  return $block;
}
