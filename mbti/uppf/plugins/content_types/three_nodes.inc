<?php

/**
 * @file
 * Content type plugin to show switcher between tinder and total list.
 *
 * The name of file should not confuse you:).
 */

$plugin = array(
  'title'           => t('Tinder switcher'),
  'description'     => t('Displays tinder switcher for employees'),
  'single'          => TRUE,
  'content_types'   => array('three_nodes'),
  'render callback' => 'three_nodes_content_type_render',
  'category'        => array(t('RAGRA'), -9),
  'edit form'       => 'three_nodes_content_type_edit_form',
);

/**
 * Settings form.
 */
function three_nodes_content_type_edit_form($form, &$form_state) {

  return $form;
}

/**
 * Settings form submit.
 */
function three_nodes_content_type_edit_form_submit($form, &$form_state) {

  foreach (element_children($form) as $key) {
    if (!empty($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Settings form validate.
 */
function three_nodes_content_type_edit_form_validate($form, &$form_state) {

}

/**
 * Render callback function.
 */
function three_nodes_content_type_render($subtype, $conf, $args, $context) {

  $block = new stdClass();

  global $user;

  $nf = FALSE;

  if (!arg(1) || !is_numeric(arg(1))) {
    $nf = TRUE;
  }
  else {
    $node = node_load(arg(1));
    if (!$node) {
      $nf = TRUE;
    }
    else {
      if ($node->uid != $user->uid && $user->uid != 1) {
        $nf = TRUE;
      }
    }
  }

  if (arg(0) == 'employees') {
    $block->content = 'Total list <a href="/employees_total/' . arg(1) . '">_</a>Tinder list';
  }
  else {
    $block->content = 'Total list <a href="/employees/' . arg(1) . '">_</a>Tinder list';
  }

  if ($nf) {
    drupal_not_found();
    return;
  }
  else {
    return $block;
  }
}
