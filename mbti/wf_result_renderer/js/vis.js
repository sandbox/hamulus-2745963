/**
 * @file
 * JS for the module.
 */

(function ($, Drupal) {
  // Use strict mode to reduce development errors.
  // @link http://www.nczonline.net/blog/2012/03/13/its-time-to-start-using-javascript-strict-mode/
  'use strict';

  Drupal.behaviors.wfr = {
    attach: function (context, settings) {

      $('.test-visibility-checkbox input').each(function () {

        $(this).once(function () {

          $(this).change(function () {
            var val = $(this).is(':checked') ? 1 : 0;

            $.get('/rset', {qnid: $(this).data('qnid'), vis: val}, function () {

            });
          });

        });

      });
    }
  };
})(jQuery, Drupal);
