/**
 * @file
 * Save alert script.
 */

(function ($, Drupal) {
  // Use strict mode to reduce development errors.
  // @link http://www.nczonline.net/blog/2012/03/13/its-time-to-start-using-javascript-strict-mode/
  'use strict';
  var nochange = false;
  Drupal.behaviors.savealert = {
    attach: function (context, settings) {

      $('input, select, textarea').change(function () {

        if (!$(this).hasClass('filter-list')) {

          nochange = true;
        }

      });

      $('.form-submit').click(function () {
        nochange = false;
      });
    }
  };

  window.onbeforeunload = function () {
    if (nochange && parseInt(Drupal.settings.savealert_uid.uid) !== 1) {

      return Drupal.t('Page contents has been changed. Do you want to leave the page without saving?');
    }
  };
})(jQuery, Drupal);
