/**
 * @file
 * Switches views page when clicking like.
 */

(function ($, Drupal) {
  // Use strict mode to reduce development errors.
  // @link http://www.nczonline.net/blog/2012/03/13/its-time-to-start-using-javascript-strict-mode/
  'use strict';
  Drupal.behaviors.fvp = {
    attach: function (context, settings) {

      $('.view-matched-vacancies .flag-outer .flag-like a, .view-matched-users .flag-outer .flag-user-like a').each(function () {

        $(this).once(function () {

          $(this).click(function () {

            var link = $(this);
            setTimeout(function () {

              link.closest('.view').find('.pager .pager-next a').click();

            }, 10);

          });

        });

      });

      $('.view-matched-vacancies .flag-outer .flag-dislike a, .view-matched-users .flag-outer .flag-employee-dislike a').each(function () {

        $(this).once(function () {

          $(this).click(function () {

            setTimeout(function () {

              $(document).ajaxStop(function () {
                $('#views-exposed-form-matched-vacancies-page .form-submit').click();
              });

            }, 10);

          });
        });
      });
    }
  };

  Drupal.behaviors.recentMeAutoRefresh = {
    attach: function (context, settings) {

      setTimeout(function () {

      }, 100);

    }
  };

})(jQuery, Drupal);
