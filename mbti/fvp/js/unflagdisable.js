/**
 * @file
 * Disables unflagging.
 */

(function ($, Drupal) {
  // Use strict mode to reduce development errors.
  // @link http://www.nczonline.net/blog/2012/03/13/its-time-to-start-using-javascript-strict-mode/
  'use strict';
  Drupal.behaviors.fvp2 = {
    attach: function (context, settings) {

      setTimeout(function () {
        $('a.unflag-action').each(function () {

          $(this).unbind('click');
          $(this).click(function (event) {
            event.preventDefault();
            event.stopPropagation();
          });
        });

        $('.node-type-vacancy .flag-dislike .flag-action').click(function () {
          window.location.href = '/vacancies';
        });

      }, 10);

    }
  };

})(jQuery, Drupal);
