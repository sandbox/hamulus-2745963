/**
 * @file
 * JS functionality for Field Permissions Custom module.
 */

(function ($, Drupal) {
  // Use strict mode to reduce development errors.
  // @link http://www.nczonline.net/blog/2012/03/13/its-time-to-start-using-javascript-strict-mode/
  'use strict';

  Drupal.behaviors.fp_custom = {
    attach: function (context, settings) {

      $('.fp-custom-processed').each(function () {

        $(this).once(function () {

          var thischeckbox = $(this).find('input[type="checkbox"]');

          thischeckbox.after('<span></span>');

        });

      });
    }
  };
})(jQuery, Drupal);
