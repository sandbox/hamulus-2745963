/**
 * @file
 * JS for scale component.
 */

(function ($, Drupal) {
  // Use strict mode to reduce development errors.
  // @link http://www.nczonline.net/blog/2012/03/13/its-time-to-start-using-javascript-strict-mode/
  'use strict';

  Drupal.behaviors.webformScale = {
    attach: function (context, settings) {

      $('.webform-client-form .form-submit:not(.webform-previous)').attr('disabled', 'disabled');

      var slcount = 0;
      var ocount = 0;
      $('.jquery-ui-scale-wrapper').each(function () {
        slcount++;
        var min = parseInt($(this).data('min'));
        var max = parseInt($(this).data('max'));
        var defaultVal = $(this).data('default');
        var thisInput = $(this).find('input');

        $(this).find('.jquery-ui-scale').click(function () {

          $(this).find('.ui-slider-handle').show();
          ocount++;

          if (ocount === slcount) {
            $('.webform-client-form .form-submit:not(.webform-previous)').removeAttr('disabled');
          }
        });

        $(this).find('.jquery-ui-scale').slider({
          orientation: 'horisontal',
          min: min,
          max: max,
          value: defaultVal,
          slide: function (event, ui) {
            thisInput.val(ui.value);
          }
        });

        if ($(this).hasClass('style-circles')) {

          var ccount = max - min + 1;
          var inner_base_max = 39;
          var inner_base_min = 18;
          var inner_base_step = (inner_base_max - inner_base_min) / (max + 1);

          $(this).find('.slider-legend-wrapper-i').append('<div class="style-circles-wrapper"></div>');
          var apos = 0;
          for (var i = min; i <= max; i++) {
            var inner_size = inner_base_min + Math.abs(i) * inner_base_step;
            $('<div class="slider-circle"><div class="slider-circle-inner" style="width:' + inner_size + 'px;height:' + inner_size + 'px;left:calc(' + ((100 / (ccount - 1)) * apos) + '% - ' + (inner_size / 2) + 'px);top:-' + (inner_size - inner_base_min) / 2 + 'px;"></div></div>').
                    appendTo($(this).find('.style-circles-wrapper')).css('width', 100 / ccount + '%');

            apos++;
          }
        }

      });

    }
  };

})(jQuery, Drupal);
