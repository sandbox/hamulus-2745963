/**
 * @file
 * JS for scale component.
 */

(function ($, Drupal) {
  // Use strict mode to reduce development errors.
  // @link http://www.nczonline.net/blog/2012/03/13/its-time-to-start-using-javascript-strict-mode/
  'use strict';

  $.extend({
    replaceTag: function (currentElem, newTagObj, keepProps) {
      var $currentElem = $(currentElem);
      var $newTag = $(newTagObj).clone();
      if (keepProps) {
        var newTag = $newTag[0];
        newTag.className = currentElem.className;
        $.extend(newTag.classList, currentElem.classList);
        $.extend(newTag.attributes, currentElem.attributes);
      }
      $currentElem.wrapAll($newTag);
      $currentElem.contents().unwrap();

      return this;
    }
  });

  $.fn.extend({
    replaceTag: function (newTagObj, keepProps) {

      return this.each(function () {
        jQuery.replaceTag(this, newTagObj, keepProps);
      });
    }
  });

  Drupal.behaviors.qselect = {
    attach: function (context, settings) {

      $('#webform-client-form-79 .webform-next').attr('disabled', 'disabled');

      $('#webform-client-form-79').once(function () {

        $(this).append('<div class="tcounter-wrapper">' + Drupal.t('Points left') + ': <span class="tcounter">10</span></div>');
      });

      $('.form-type-radios').each(function () {
        $(this).once(function () {

          $(this).find('input').change(function () {
            var val = parseInt($(this).val());

            var thisparent = $(this).closest('.form-type-radios');

            var total_left = 10 - val;

            $('.form-type-radios').not(thisparent).each(function () {
              var thisval = parseInt($(this).find(':checked').val());

              if (!isNaN(thisval)) {
                total_left = total_left - thisval;
              }

            });

            $('.tcounter').html(total_left);

            if (total_left > 0) {
              $('#webform-client-form-79 .webform-next').attr('disabled', 'disabled');
            }
            else {

              $('#webform-client-form-79 .webform-next').removeAttr('disabled');
            }

            $('.form-type-radios').find('input').each(function () {
              var thisval_option = parseInt($(this).attr('value'));

              var thisrow_selected = parseInt($(this).closest('.form-group').find(':checked').val());

              if (thisval_option > total_left && (total_left + thisrow_selected - thisval_option) < 0) {

                $(this).attr('disabled', 'disabled');
              }
              else {

                $(this).removeAttr('disabled');

              }

            });

          });
        });
      });
    }
  };

})(jQuery, Drupal);
