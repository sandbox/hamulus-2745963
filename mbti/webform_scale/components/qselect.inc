<?php

/**
 * @file
 * Webform Link component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_qselect() {
  return array(
    'name'     => '',
    'form_key' => NULL,
    'pid'      => 0,
    'weight'   => 0,
    'value'    => '',
    'required' => 0,
    'extra'    => array(
      'private'     => FALSE,
      'description' => '',
      'options'     => '',
      'axis'        => '',
      'max_value'   => 1,
      'min_value'   => 0,
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_qselect($component) {

  $form = array();

  $form['value'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Default value'),
    '#default_value' => $component['value'] ? $component['value'] : 0,
    '#description'   => t('The default value of the field.') . theme('webform_token_help'),
    '#size'          => 60,
    '#maxlength'     => 1024,
    '#weight'        => 0,
  );

  $form['options'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Select options'),
    '#default_value' => !empty($component['extra']['options']) ? $component['extra']['options']
    : '',
    '#description'   => t('Select options') . theme('webform_token_help'),
    '#size'          => 60,
    '#maxlength'     => 1024,
    '#weight'        => 5,
    '#parents'       => array('extra', 'options'),
  );

  $form['axis'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Axis name'),
    '#default_value' => $component['extra']['axis'] ? $component['extra']['axis']
    : '',
    '#description'   => t('Axis machine name.') . theme('webform_token_help'),
    '#size'          => 60,
    '#maxlength'     => 1024,
    '#weight'        => 5,
    '#parents'       => array('extra', 'axis'),
  );

  $form['min_value'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Min value'),
    '#default_value' => isset($component['extra']['min_value']) ? $component['extra']['min_value']
    : -1,
    '#description'   => t('The minimum value of the scale.') . theme('webform_token_help'),
    '#size'          => 60,
    '#maxlength'     => 1024,
    '#weight'        => 1,
    '#parents'       => array('extra', 'min_value'),
  );

  $form['max_value'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Max value'),
    '#default_value' => $component['extra']['max_value'] ? $component['extra']['max_value']
    : 1,
    '#description'   => t('The maximum value of the scale.') . theme('webform_token_help'),
    '#size'          => 60,
    '#maxlength'     => 1024,
    '#weight'        => 3,
    '#parents'       => array('extra', 'max_value'),
  );

  $form['description']['#type'] = 'hidden';

  return $form;
}

/**
 * Settings form validation.
 */
function _webform_edit_qselect_validate($form, &$form_state) {

}

/**
 * Implements _webform_theme_component().
 */
function _webform_theme_qselect() {
  return array(
    'webform_display_qselect' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_qselect($component, $value = NULL, $filter = TRUE) {

  drupal_add_library('system', 'ui');
  drupal_add_library('system', 'ui.selectmenu');

  drupal_add_js(drupal_get_path('module', 'webform_scale') . '/js/qselect.js');
  drupal_add_css(drupal_get_path('module', 'webform_scale') . '/css/qselect.css');

  $options_pre = explode("\n", $component['extra']['options']);

  $options = array();
  foreach ($options_pre as $option) {
    $option_ar = explode('|', $option);
    if (isset($option_ar[1])) {
      $options[trim($option_ar[0])] = trim($option_ar[1]);
    }
    else {
      $options[trim($option_ar[0])] = trim($option_ar[0]);
    }
  }

  $element = array(
    '#title'   => $filter ? _webform_filter_xss($component['name']) : $component['name'],
    '#type'    => 'radios',
    '#options' => $options,
    '#weight'  => $component['weight'],
    '#title_display'    => $component['extra']['title_display'] ? $component['extra']['title_display']
    : 'before',
    '#default_value'    => $component['value'],
    '#required'         => $component['required'],
    '#translatable'     => array('title', 'description'),
    '#theme_wrappers'   => array('webform_element'),
    '#element_validate' => array('webform_qselect_validate'),
  );

  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_qselect($component, $value, $format = 'html') {

  $element = array(
    '#title'          => $component['name'],
    '#weight'         => $component['weight'],
    '#theme'          => 'webform_display_qselect',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#format'         => $format,
    '#value'          => $value[0],
    '#translatable'   => array('title'),
  );
  return $element;
}

/**
 * Custom theme function for collected link.
 */
function theme_webform_display_qselect($variables) {
  $element = $variables['element'];
  $value = $element['#value'];
  return $value;
}

/**
 * Implements _webform_table_component().
 */
function _webform_table_qselect($component, $value) {
  return $value[0];
}

/**
 * Implements _webform_csv_headers_component().
 */
function _webform_csv_headers_qselect($component, $export_options) {
  $header = array();
  $header[0] = '';
  $header[1] = '';
  $header[2] = $component['name'];
  return $header;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_qselect($component, $export_options, $value) {
  return $value[0];
}

/**
 * Form validation handler.
 *
 * If a link was submitted, check that the URL is valid.
 */
function webform_qselect_validate($element, &$form_state) {

}
