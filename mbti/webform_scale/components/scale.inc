<?php

/**
 * @file
 * Webform Scale component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_scale() {
  return array(
    'name'     => '',
    'form_key' => NULL,
    'pid'      => 0,
    'weight'   => 0,
    'value'    => '',
    'required' => 0,
    'extra'    => array(
      'private'      => FALSE,
      'description'  => '',
      'max_value'    => 1,
      'max_title'    => '',
      'min_value'    => -1,
      'min_title'    => '',
      'slider_style' => 'circles',
      'axis'         => '',
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_scale($component) {

  $form = array();

  $form['value'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Default value'),
    '#default_value' => $component['value'] ? $component['value'] : 0,
    '#description'   => t('The default value of the field.') . theme('webform_token_help'),
    '#size'          => 60,
    '#maxlength'     => 1024,
    '#weight'        => 0,
  );

  $form['min_value'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Min value'),
    '#default_value' => $component['extra']['min_value'] ? $component['extra']['min_value']
    : -1,
    '#description'   => t('The minimum value of the scale.') . theme('webform_token_help'),
    '#size'          => 60,
    '#maxlength'     => 1024,
    '#weight'        => 1,
    '#parents'       => array('extra', 'min_value'),
  );

  $form['min_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Min title'),
    '#default_value' => $component['extra']['min_title'] ? $component['extra']['min_title']
    : '',
    '#description'   => t('Title of minimum value') . theme('webform_token_help'),
    '#size'          => 60,
    '#maxlength'     => 1024,
    '#weight'        => 2,
    '#parents'       => array('extra', 'min_title'),
  );

  $form['max_value'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Max value'),
    '#default_value' => $component['extra']['max_value'] ? $component['extra']['max_value']
    : 1,
    '#description'   => t('The maximum value of the scale.') . theme('webform_token_help'),
    '#size'          => 60,
    '#maxlength'     => 1024,
    '#weight'        => 3,
    '#parents'       => array('extra', 'max_value'),
  );

  $form['max_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Max title'),
    '#default_value' => $component['extra']['max_title'] ? $component['extra']['max_title']
    : '',
    '#description'   => t('Title of maximum value') . theme('webform_token_help'),
    '#size'          => 60,
    '#maxlength'     => 1024,
    '#weight'        => 4,
    '#parents'       => array('extra', 'max_title'),
  );

  $form['axis'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Axis name'),
    '#default_value' => $component['extra']['axis'] ? $component['extra']['axis']
    : '',
    '#description'   => t('Axis machine name.') . theme('webform_token_help'),
    '#size'          => 60,
    '#maxlength'     => 1024,
    '#weight'        => 5,
    '#parents'       => array('extra', 'axis'),
  );

  $form['slider_style'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Slider style'),
    '#default_value' => $component['extra']['slider_style'] ? $component['extra']['slider_style']
    : 'circles',
    '#description'   => t('Slider style'),
    '#options'       => array('circles' => t('Circles'), 'classic' => t('Classic')),
    '#weight'  => 6,
    '#parents' => array('extra', 'slider_style'),
  );

  $form['description']['#type'] = 'hidden';

  return $form;
}

/**
 * Validate form.
 */
function _webform_edit_scale_validate($form, &$form_state) {

}

/**
 * Implements _webform_theme_component().
 */
function _webform_theme_scale() {
  return array(
    'webform_display_scale' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_scale($component, $value = NULL, $filter = TRUE) {

  drupal_add_library('system', 'ui');
  drupal_add_library('system', 'ui.slider');

  drupal_add_js(drupal_get_path('module', 'webform_scale') . '/js/scale.js');
  drupal_add_css(drupal_get_path('module', 'webform_scale') . '/css/scale.css');

  $style = 'circles';

  if (isset($component['extra']['slider_style'])) {
    $style = $component['extra']['slider_style'];
  }
  $element = array(
    '#type'             => 'hidden',
    '#title'            => $filter ? _webform_filter_xss($component['name']) : $component['name'],
    '#title_display'    => $component['extra']['title_display'] ? $component['extra']['title_display']
    : 'before',
    '#default_value'    => $component['value'],
    '#required'         => $component['required'],
    '#weight'           => $component['weight'],
    '#description'      => $filter ? _webform_filter_xss($component['extra']['description'])
    : $component['extra']['description'],
    '#translatable'     => array('title', 'description'),
    '#theme_wrappers'   => array('webform_element'),
    '#element_validate' => array('webform_scale_validate'),
    '#attributes'       => array('class' => array('jquery-ui-scale')),
    '#prefix'           => '<div class="style-' . $style . ' jquery-ui-scale-wrapper" data-min="' . $component['extra']['min_value'] . '" data-max="' . $component['extra']['max_value'] . '" data-default="' . $component['value'] . '"><div class="slider-legend-wrapper-i clearfix">',
    '#suffix'           => '<div class="slider-legend-wrapper clearfix"><div class="slider-legend-left">' . $component['extra']['min_title'] . '</div><div class="slider-legend-right">' . $component['extra']['max_title'] . '</div></div><div class="jquery-ui-scale"></div></div></div>',
  );
  if (isset($value[0])) {
    $element['#default_value'] = $value[0];
  }
  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_scale($component, $value, $format = 'html') {

  $element = array(
    '#title'          => $component['name'],
    '#weight'         => $component['weight'],
    '#theme'          => 'webform_display_scale',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#format'         => $format,
    '#value'          => $value[0],
    '#translatable'   => array('title'),
  );
  return $element;
}

/**
 * Custom theme function for collected link.
 */
function theme_webform_display_scale($variables) {
  $element = $variables['element'];
  $value = $element['#value'];
  return $value;
}

/**
 * Implements _webform_table_component().
 */
function _webform_table_scale($component, $value) {
  return $value[0];
}

/**
 * Implements _webform_csv_headers_component().
 */
function _webform_csv_headers_scale($component, $export_options) {
  $header = array();
  $header[0] = '';
  $header[1] = '';
  $header[2] = $component['name'];
  return $header;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_scale($component, $export_options, $value) {
  return $value[0];
}

/**
 * Form validation handler.
 *
 * If a link was submitted, check that the URL is valid.
 */
function webform_scale_validate($element, &$form_state) {

  if (!is_numeric($element['#value']) || $element['#value'] < $element['#webform_component']['extra']['min_value'] ||
      $element['#value'] > $element['#webform_component']['extra']['max_value']) {
    $message = t('%number is incorrect value',
        array('%number' => $element['#value']));
    form_error($element, $message);
  }
}
